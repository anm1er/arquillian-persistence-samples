package org.samples.arquillian.extensions.persistence.repository;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.CleanupUsingScript;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.samples.arquillian.extensions.persistence.model.Speaker;
import org.samples.arquillian.extensions.persistence.model.Talk;
import org.samples.arquillian.extensions.persistence.model.Track;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
@CleanupUsingScript("drop-schema.sql")
public class TalkRepositoryTest {

    public static final int TALKS_NUMBER = 7;

    public static final String SERVER_SIDE_TACK_TITLE = "Server Side Java";
    public static final long SPRING_DATA_TALK_ID = 6L;
    public static final String SPRING_DATA_TALK = "What's new in Spring Data?";
    public static final String SPRING_SPEAKER_NAME = "Oliver";
    public static final String SPRING_SPEAKER_LASTNAME = "Gierke";

    public static final long MVC_TALK_ID = 5L;
    public static final String MVC_TALK_NAME = "MVC 1.0 - by Example";

    public static final String JIGSAW_TALK_TITLE = "Project Jigsaw: Under the Hood";
    public static final String JIGSAW_SPEAKER_NAME_1 = "Mark";
    public static final String JIGSAW_SPEAKER_LASTNAME_1 = "Reinhold";
    public static final String JIGSAW_SPEAKER_NAME_2 = "Alex";
    public static final String JIGSAW_SPEAKER_LASTNAME_2 = "Buckley";
    public static final String JIGSAW_SPEAKER_NAME_3 = "Alan";
    public static final String JIGSAW_SPEAKER_LASTNAME_3 = "Bateman";

    @Deployment
    public static Archive<?> createArchive() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Talk.class.getPackage())
                .addPackage(TalkRepository.class.getPackage())
                .addPackages(true, "org.assertj")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("test-ds.xml");
    }

    @Inject
    private TalkRepository talkRepository;

    @Test
    // Uses DBUnit approach to seed database
    @UsingDataSet("talks.yml")
    public void findAll_ShouldReturnAllTalks() {
        List<Talk> talkList = talkRepository.findAll();

        assertEquals(TALKS_NUMBER, talkList.size());
    }

    @Test
    @UsingDataSet({"talks.yml", "speakers.yml"})
    public void findOneByTitle_SpringDataTalkTitleGiven_ShouldReturnOneTalk() {
        Talk talk = talkRepository.findOneByTitle(SPRING_DATA_TALK);

        assertEquals(1, talk.getSpeakers().size());
        assertThat(talk.getSpeakers()).extracting("firstName", "lastName")
                .contains(tuple(SPRING_SPEAKER_NAME, SPRING_SPEAKER_LASTNAME));
    }

    @Test
    @UsingDataSet("talks-with-tracks.yml")
    public void findByTrack_PersistentServerSideJavaTrackGiven_ShouldReturnTwoTalks() {
        Talk talk = talkRepository.findOne(SPRING_DATA_TALK_ID);
        Track serverSideTrack = talk.getTrack();
        assertNotNull(serverSideTrack);
        assertEquals(SERVER_SIDE_TACK_TITLE, serverSideTrack.getTitle());

        List<Talk> talkList = talkRepository.findByTrack(serverSideTrack);

        assertThat(talkList).isNotEmpty();
        assertThat(talkList).contains(talk);
        assertThat(talkList).extracting("id", "title")
                .contains(tuple(MVC_TALK_ID, MVC_TALK_NAME))
                .contains(tuple(SPRING_DATA_TALK_ID, SPRING_DATA_TALK));
    }

    @Test
    @UsingDataSet("talks-with-tracks.yml")
    public void findByTrackTitle_ServerSideJavaTrackTitleGiven_ShouldReturnTwoTalks() {
        List<Talk> talkList = talkRepository.findByTrackTitle(SERVER_SIDE_TACK_TITLE);

        assertThat(talkList).isNotEmpty();
        assertEquals(2, talkList.size());
        assertThat(talkList).extracting("id", "title")
                .contains(tuple(MVC_TALK_ID, MVC_TALK_NAME))
                .contains(tuple(SPRING_DATA_TALK_ID, SPRING_DATA_TALK));
    }

    @Test
    @UsingDataSet({"talks.yml", "speakers.yml"})
    public void findTalksBySpeaker_JigsawSpeakerGiven_ShouldReturnOneJigsawTalk() {
        Set<Speaker> jigsawSpeakers = talkRepository.findOneByTitle(JIGSAW_TALK_TITLE).getSpeakers();
        List<Talk> talksBySpeaker = talkRepository.findTalksBySpeaker(jigsawSpeakers.iterator().next());

        assertThat(talksBySpeaker).isNotEmpty();
        assertEquals(1, talksBySpeaker.size());
        assertThat(talksBySpeaker.get(0).getSpeakers()).extracting("firstName", "lastName")
                .contains(tuple(JIGSAW_SPEAKER_NAME_1, JIGSAW_SPEAKER_LASTNAME_1))
                .contains(tuple(JIGSAW_SPEAKER_NAME_2, JIGSAW_SPEAKER_LASTNAME_2))
                .contains(tuple(JIGSAW_SPEAKER_NAME_3, JIGSAW_SPEAKER_LASTNAME_3));
    }

    @Test
    @UsingDataSet({"talks.yml", "speakers.yml"})
    public void findTalkTitlesWithNSpeakers_3Given_ShouldReturnJigsawTalkTitle() {
        List<String> talkWithNSpeakers = talkRepository.findTalkTitlesWithNSpeakers(3);

        assertThat(talkWithNSpeakers).isNotEmpty();
        assertEquals(1, talkWithNSpeakers.size());
        assertThat(talkWithNSpeakers).contains(JIGSAW_TALK_TITLE);
    }

}
