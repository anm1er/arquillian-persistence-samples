package org.samples.arquillian.extensions.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.persistence.CleanupUsingScript;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.samples.arquillian.extensions.persistence.model.Language;
import org.samples.arquillian.extensions.persistence.model.Speaker;
import org.samples.arquillian.extensions.persistence.model.Talk;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class TalkPersistenceTest {

    public static final String SPEAKER_FIRST_NAME_1 = "Andy";
    public static final String SPEAKER_LAST_NAME_1 = "Petrella";
    public static final String SPEAKER_FIRST_NAME_2 = "Xavier";
    public static final String SPEAKER_LAST_NAME_2 = "Tordoir";
    public static final String TALK_TITLE = "Towards a rebirth of Data Science";

    @Deployment
    public static Archive<?> createArchive() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Talk.class.getPackage())
                .addPackages(true, "org.assertj")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("test-ds.xml");
    }

    @PersistenceContext
    private EntityManager em;

    @Test
    @InSequence(1)
    // By default each Arquillian test with tx support finishes with commit operation
    @Transactional(TransactionMode.ROLLBACK)
    public void shouldPersistTalkWithSpeakers() {
        Speaker speaker1 = new Speaker(SPEAKER_FIRST_NAME_1, SPEAKER_LAST_NAME_1);
        Speaker speaker2 = new Speaker(SPEAKER_FIRST_NAME_2, SPEAKER_LAST_NAME_2);
        Talk talk = new Talk(TALK_TITLE, Language.ENGLISH);
        talk.addSpeakers(speaker1, speaker2);

        em.persist(talk);
        em.flush();
        em.clear();
        @SuppressWarnings("unchecked")
        List<Talk> talkList = em.createQuery("from Talk")
                .getResultList();

        assertEquals(1, talkList.size());
        assertThat(talkList.get(0)).isEqualTo(talk);
        assertEquals(2, talkList.get(0).getSpeakers().size());
        assertThat(talkList.get(0).getSpeakers()).extracting("firstName", "lastName")
                .contains(tuple(SPEAKER_FIRST_NAME_1, SPEAKER_LAST_NAME_1))
                .contains(tuple(SPEAKER_FIRST_NAME_2, SPEAKER_LAST_NAME_2));
    }

    @Test
    @InSequence(2)
    // Uses the DBUnit datasets approach to validate the database state
    @ShouldMatchDataSet("datasets/talk-speakers.yml")
    // By default the database is entirely erased before each test
    // Custom SQL scripts can be used to clean the database before or after the test
    @CleanupUsingScript("drop-schema.sql")
    public void shouldPersistTalkWithSpeakersAndVerifyStateWithDatasetFile() {
        Speaker speaker1 = new Speaker(SPEAKER_FIRST_NAME_1, SPEAKER_LAST_NAME_1);
        Speaker speaker2 = new Speaker(SPEAKER_FIRST_NAME_2, SPEAKER_LAST_NAME_2);
        Talk talk = new Talk(TALK_TITLE, Language.ENGLISH);
        talk.addSpeakers(speaker1, speaker2);

        em.persist(talk);
    }

}
