package org.samples.arquillian.extensions.persistence.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.samples.arquillian.extensions.persistence.model.Speaker;
import org.samples.arquillian.extensions.persistence.model.Speaker_;
import org.samples.arquillian.extensions.persistence.model.Talk;
import org.samples.arquillian.extensions.persistence.model.Talk_;
import org.samples.arquillian.extensions.persistence.model.Track;

public class TalkRepository extends PersistenceRepository<Talk> {

    public TalkRepository() {
        super(Talk.class);
    }

    public Talk findOneByTitle(String title) {
        return getEntityManager()
                .createQuery("from Talk t WHERE t.title=:title", Talk.class)
                .setParameter("title", title)
                .getSingleResult();
    }

    public List<Talk> findByTrack(Track track) {
        return getEntityManager()
                .createQuery("from Talk t WHERE t.track=:track", Talk.class)
                .setParameter("track", track)
                .getResultList();
    }

    public List<Talk> findByTrackTitle(String trackTitle) {
        return getEntityManager()
                .createQuery("from Talk t WHERE t.track.title =:trackTitle", Talk.class)
                .setParameter("trackTitle", trackTitle)
                .getResultList();
    }

    public List<String> findTalkTitlesWithNSpeakers(int speakersNumber) {
        List<Object[]> resultList = getEntityManager()
                .createQuery("select t.title, count(s) from Talk t join t.speakers s "
                        + "group by t.title having count(s) = :speakersNumber", Object[].class)
                .setParameter("speakersNumber", new Long(speakersNumber))
                .getResultList();
        List<String> talkTitles = new ArrayList<>(resultList.size());
        for (Object[] result : resultList) {
            talkTitles.add((String) result[0]);
        }
        return talkTitles;
    }

    public List<Talk> findTalksBySpeaker(Speaker speaker) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Talk> talksQuery = criteriaBuilder.createQuery(Talk.class);
        Root<Talk> fromTalk = talksQuery.from(Talk.class);
        Join<Talk, Speaker> speakers = fromTalk.join(Talk_.speakers);
        CriteriaQuery<Talk> talksWithSpeakerQuery = talksQuery.select(fromTalk).where(
                criteriaBuilder.equal(speakers.get(Speaker_.id), speaker.getId()));
        return getEntityManager().createQuery(talksWithSpeakerQuery).getResultList();
    }

}
