package org.samples.arquillian.extensions.persistence.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings("serial")
@Entity
public class Talk extends BaseEntity {

    @NotNull
    @Size(min = 10, max = 100)
    private String title;

    @Enumerated(EnumType.ORDINAL)
    private Language language;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Speaker> speakers = new HashSet<Speaker>();

    @ManyToOne
    @JoinColumn
    private Track track;

    public Talk() {
    }

    public Talk(String title) {
        this.title = title;
    }

    public Talk(String title, Language language) {
        this.title = title;
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<Speaker> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(Set<Speaker> speakers) {
        this.speakers = speakers;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public void addSpeakers(Speaker... speakers) {
        this.speakers.addAll(Arrays.asList(speakers));
        for (Speaker speaker : speakers) {
            speaker.addTalk(this);
        }
    }

    public void addSpeaker(Speaker speaker) {
        // Check required to avoid nested recursive calls
        if (!speakers.contains(speaker)) {
            speakers.add(speaker);
            speaker.addTalk(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Talk talk = (Talk) o;
        return Objects.equals(title, talk.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Talk{");
        sb.append("title='").append(title).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
