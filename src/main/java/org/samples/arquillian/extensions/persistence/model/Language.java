package org.samples.arquillian.extensions.persistence.model;

public enum Language {

    ENGLISH,
    FRENCH

}
