# Arquillian Peristence Extension Samples

The samples are configured to run in either **WildFly 8 managed** or **WildFly 8 remote** container.
They should be ported to arquillian-universe and use [Chameleon container adapter].

------

## Note
It looks like there are issues using the latest (1.1.9.Final) core arquillian version with persistence extension.
(In fact, folks in stackoverflow discussions reported any 1.1.2.Final+ versions to be buggy with DBUnit)



[Chameleon container adapter]: http://arquillian.org/modules/arquillian-container-chameleon-container-adapter/
